## Resto API

Instalasi : 
- composer install
- copy .env.example -> .env
- setting database
- php artisan key:generate
- php artisan migrate
- php artisan db:seed
- php artisan serve 

### DATA MENU
#### Ambil Data Menu
GET /api/menu

parameter tambahahan :
* categories = filter by id categories
* active = filter by is_active

#### Buat Data Menu
POST /api/menu

parameter :
* id_categories
* name
* desc
* price
* is_active

#### Ambil 1 Data Menu
GET /api/menu/:id

#### Update Data Menu
POST /api/menu/:id/update

paramter:
* name
* desc
* price
* is_active

#### Hapus Data Menu
POST /api/menu/:id/delete


### DATA KATEGORI
#### Ambil Data categories
GET /api/categories

#### Buat Data categories
POST /api/categories

parameter :
* name
* desc

#### Ambil 1 Data categories
GET /api/categories/:id

#### Update Data categories
POST /api/categories/:id/update

paramter:
* name
* desc

#### Hapus Data categories
POST /api/categories/:id/delete
