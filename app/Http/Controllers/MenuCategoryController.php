<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseFormatter;
use App\Models\MenuCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use function PHPUnit\Framework\isEmpty;

class MenuCategoryController extends Controller
{
    public function all(Request $request)
    {
        $categories = MenuCategory::all();

        if ($categories->count() <= 0) {
            return ResponseFormatter::error(null, 'Data Kosong');
        }
        return ResponseFormatter::success($categories, 'Data Berhasil Diambil');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'desc' => 'required',
        ]);

        if ($validator->fails()) {
            return ResponseFormatter::error($validator->errors()->all(), 'Gagal Menyimpan');
        }

        $categories = MenuCategory::create([
            'name' => $request->name,
            'desc' => $request->desc,
        ]);
        $categories->save();

        return ResponseFormatter::success($categories, 'Data "' . $request->name . '" Berhasil Disimpan');
    }

    public function show($id, Request $request)
    {
        $categories = MenuCategory::find($id);

        if (!$categories) {
            return ResponseFormatter::error(null, 'Data Tidak Ditemukan');
        }
        return ResponseFormatter::success($categories, 'Data Berhasil Diambil');
    }

    public function update($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'desc' => 'required',
        ]);

        if ($validator->fails()) {
            return ResponseFormatter::error($validator->errors()->all(), 'Gagal Menyimpan');
        }

        $categories = MenuCategory::find($id);
        if (!$categories) {
            return ResponseFormatter::error(null, 'Data Tidak Ditemukan');
        }

        $categories->update([
            'name' => $request->name,
            'desc' => $request->desc,
        ]);
        $categories->save();

        return ResponseFormatter::success($categories, 'Data "' . $request->name . '" Berhasil Disimpan');
    }
    public function delete($id)
    {
        $categories = MenuCategory::find($id);

        if (!$categories) {
            return ResponseFormatter::error(null, 'Data Tidak Ditemukan');
        }
        $nama = $categories->name;
        $categories->delete();
        return ResponseFormatter::success(null, 'Data "' . $nama . '" Berhasil Dihapus');
    }
}
