<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseFormatter;
use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use function PHPUnit\Framework\isEmpty;

class MenuController extends Controller
{
    public function all(Request $request)
    {
        $menu = Menu::with('categories:id,name,desc');

        if ($request->has('categories')) {
            $menu = $menu->where('id_categories', $request->categories);
        }

        if ($request->has('active')) {
            $menu = $menu->where('is_active', $request->active);
        }

        if ($request->has('limit')) {
            $menu = $menu->paginate($request->limit);
        } else {
            $menu = $menu->get();
        }

        if ($menu->count() <= 0) {
            return ResponseFormatter::error(null, 'Data Menu Kosong');
        }
        return ResponseFormatter::success($menu, 'Data Menu Berhasil Diambil');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_categories' => 'required',
            'name' => 'required',
            'desc' => 'required',
            'price' => 'required'
        ]);

        if ($validator->fails()) {
            return ResponseFormatter::error($validator->errors()->all(), 'Gagal Menyimpan');
        }

        $menu = Menu::create([
            'id_categories' => $request->id_categories,
            'name' => $request->name,
            'desc' => $request->desc,
            'price' => $request->price,
        ]);

        if ($request->has('is_active')) {
            $menu->is_active = $request->is_active;
        }

        $menu->save();

        return ResponseFormatter::success($menu, 'Data "' . $request->name . '" Berhasil Disimpan');
    }

    public function show($id, Request $request)
    {
        $menu = Menu::find($id);

        if (!($menu)) {
            return ResponseFormatter::error($menu, 'Data Menu Tidak Ditemukan');
        }
        return ResponseFormatter::success($menu, 'Data Menu Berhasil Diambil');
    }

    public function update($id, Request $request)
    {
        // $validator = Validator::make($request->all(), [
        //     'id_categories' => 'required',
        //     'name' => 'required',
        //     'desc' => 'required',
        //     'price' => 'required'
        // ]);

        // if ($validator->fails()) {
        //     return ResponseFormatter::error($validator->errors()->all(), 'Gagal Menyimpan');
        // }

        $menu = Menu::find($id);
        if (!($menu)) {
            return ResponseFormatter::error(null, 'Data Menu Tidak Ditemukan');
        }

        $menu->update($request->all());
        $menu->save();

        return ResponseFormatter::success($menu, 'Data "' . $request->name . '" Berhasil Disimpan');
    }
    public function delete($id)
    {
        $menu = Menu::find($id);

        if (!($menu)) {
            return ResponseFormatter::error(null, 'Data Menu Tidak Ditemukan');
        }
        $nama = $menu->name;
        $menu->delete();
        return ResponseFormatter::success(null, 'Data "' . $nama . '" Berhasil Dihapus');
    }
}
