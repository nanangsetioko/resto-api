<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseFormatter;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function login(Request $request)
    {
        try {
            $request->validate([
                'email' => 'required',
                'password' => 'required'
            ]);

            $credentials = request(['email', 'password']);

            if (!Auth::attempt($credentials)) {
                return ResponseFormatter::error(null, 'Login Gagal', 200);
            }


            $user = User::where('email', $request->email)->first();

            if (!Hash::check($request->password, $user->password, [])) {
                throw new \Exception('Invalid Credentials');
            }

            $tokenResult = $user->createToken('authToken')->plainTextToken;
            return ResponseFormatter::success([
                'access_token' => $tokenResult,
                'token_type' => 'Bearer',
                'identity' => '-',
                'user' => $user,
            ], 'Login Berhasil');
        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
            ], 'Login Gagal', 200);
        }
    }

    public function updateProfile(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();

        $user->update($data);

        return ResponseFormatter::success($user, 'Profil berhasil di update');
    }


    public function updateUsername(Request $request)
    {
        $username = $request->input('username');
        $old_username = $request->input('old_username');
        $password = $request->input('password');

        $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        $user = Auth::user();

        if ($user->username == $old_username) {
            if (Hash::check($password, $user->password, [])) {
                $user->update([
                    'username' => $username,
                ]);
            } else {
                return ResponseFormatter::error([
                    null,
                ], 'Password tidak sama', 200);
            }
        } else {
            return ResponseFormatter::error([
                null,
            ], 'Username tidak sama', 200);
        }


        return ResponseFormatter::success($user, 'Username berhasil di update');
    }

    public function updatePassword(Request $request)
    {
        $password = $request->input('password');
        $new_password = $request->input('new_password');

        $request->validate([
            'new_password' => 'required',
            'password' => 'required'
        ]);

        $user = Auth::user();

        if (Hash::check($password, $user->password, [])) {
            $user->update([
                'password' => Hash::make($new_password),
            ]);
        } else {
            return ResponseFormatter::error([
                null,
            ], 'Password salah', 200);
        }


        return ResponseFormatter::success($user, 'Password berhasil di update');
    }

    public function logout(Request $request)
    {
        $token = $request->user()->currentAccessToken()->delete();

        return ResponseFormatter::success($token, 'Berhasil Logout');
    }
}
