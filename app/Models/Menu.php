<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;

    protected $fillable = ['id_categories', 'name', 'desc', 'price', 'is_active'];

    public function categories()
    {
        return $this->hasOne(MenuCategory::class, 'id', 'id_categories');
    }
}
