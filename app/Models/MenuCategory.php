<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MenuCategory extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'desc'];

    public function menu()
    {
        return $this->belongsTo(Menu::class, 'id_categories', 'id');
    }
}
