<?php

namespace Database\Seeders;

use App\Models\Menu;
use App\Models\MenuCategory;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        MenuCategory::create([
            'name' => 'Makanan',
            'desc' => '-'
        ])->save();

        MenuCategory::create([
            'name' => 'Minuman',
            'desc' => '-'
        ])->save();

        Menu::create([
            'id_categories' => 1,
            'name' => 'Nasi Kucing',
            'desc' => 'Nasi dengan bandeng dibungkus dengan kertas ujian..',
            'price' => 2000
        ]);
        Menu::create([
            'id_categories' => 1,
            'name' => 'Nasi Goreng',
            'desc' => 'Nasi yang digoreng..',
            'price' => 10000
        ]);
        Menu::create([
            'id_categories' => 1,
            'name' => 'Nasi Liwet',
            'desc' => 'Nasi yang diliwet..',
            'price' => 5000
        ]);

        Menu::create([
            'id_categories' => 2,
            'name' => 'Es Teh',
            'desc' => 'Teh dengan Es..',
            'price' => 2000
        ]);
        Menu::create([
            'id_categories' => 2,
            'name' => 'Es Good Day',
            'desc' => 'Kopi Good Day diberi es..',
            'price' => 2500
        ]);

        User::create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('adminrhs'),
        ]);
    }
}
