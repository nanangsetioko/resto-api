<?php

use App\Http\Controllers\MenuCategoryController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', [UserController::class, 'login']);


Route::get('menu', [MenuController::class, 'all']);
Route::get('menu/{id}', [MenuController::class, 'show']);
Route::post('menu/{id}/update', [MenuController::class, 'update']);
Route::post('menu/{id}/delete', [MenuController::class, 'delete']);
Route::post('menu', [MenuController::class, 'store']);


Route::get('categories', [MenuCategoryController::class, 'all']);
Route::get('categories/{id}', [MenuCategoryController::class, 'show']);
Route::post('categories/{id}/update', [MenuCategoryController::class, 'update']);
Route::post('categories/{id}/delete', [MenuCategoryController::class, 'delete']);
Route::post('categories', [MenuCategoryController::class, 'store']);



Route::middleware('auth:sanctum')->prefix('admin')->group(function () {
    Route::get('menu', [MenuController::class, 'all']);
    Route::get('menu/{id}', [MenuController::class, 'show']);
    Route::post('menu/{id}/update', [MenuController::class, 'update']);
    Route::post('menu/{id}/delete', [MenuController::class, 'delete']);
    Route::post('menu', [MenuController::class, 'store']);


    Route::get('categories', [MenuCategoryController::class, 'all']);
    Route::get('categories/{id}', [MenuCategoryController::class, 'show']);
    Route::post('categories/{id}/update', [MenuCategoryController::class, 'update']);
    Route::post('categories/{id}/delete', [MenuCategoryController::class, 'delete']);
    Route::post('categories', [MenuCategoryController::class, 'store']);
});
